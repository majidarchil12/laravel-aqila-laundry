<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superuser = User::create([
            'username' => 'superuser',
            'name' => 'Superuser',
            'password' => Hash::make('superaqila'),
            'pwd' => 'superaqila',
        ]);

        $superuser->assignRole('superuser');

        $admin = User::create([
            'username' => 'admin',
            'name' => 'Admin',
            'password' => Hash::make('Aqila123'),
            'pwd' => 'Aqila123',
        ]);

        $admin->assignRole('admin');
    }
}
