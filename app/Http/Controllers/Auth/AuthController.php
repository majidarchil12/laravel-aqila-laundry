<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    function index()
    {
        return view('pages.auth.login.index');
    }

    function checkLogin()
    {
        return redirect('/admin');
    }

    function logout() {
        return redirect('/');
    }
}
