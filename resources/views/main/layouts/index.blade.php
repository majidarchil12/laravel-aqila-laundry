<!doctype html>
<html data-theme="light">

<head>
    @include('main.core.meta.index')

    <title>Aqila Laundry | @yield('title')</title>

    @vite('resources/css/app.css')

    @include('main.core.css.index')

    @yield('css')
</head>

<body>



    <div class="container mx-auto my-auto">
        @if (Route::currentRouteName() != 'login')
            @include('components.navbar.index')
        @endif
        @yield('content')
    </div>

    @include('main.core.js.index')

    @yield('js')
</body>

</html>
