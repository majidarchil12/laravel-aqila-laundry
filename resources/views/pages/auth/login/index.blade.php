@extends('main.layouts.index')

@section('title')
    Login
@endsection

@section('content')
    <div class="hero min-h-screen bg-base-200">
        <div class="hero-content flex-col lg:flex-row-reverse">
            <div class="text-center lg:text-left">
                <h1 class="text-4xl font-bold text-center">Aqila Laundry</h1>
                <p class="py-6">
                    <img alt="Logo Aqila" id="logo-aqila" class="w-96">
                </p>
            </div>
            <div class="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
                <div class="card-body">
                    <form action="{{ route('check-login') }}" method="POST">
                        @csrf

                        <div class="form-control">
                            <label class="label">
                                <span class="label-text">Username</span>
                            </label>
                            <input type="text" name="username" placeholder="Username" class="input input-bordered" />
                        </div>
                        <div class="form-control">
                            <label class="label">
                                <span class="label-text">Password</span>
                            </label>
                            <input type="password" name="password" placeholder="Password" class="input input-bordered" />
                            {{-- <label class="label">
                            <a href="#" class="label-text-alt link link-hover">Forgot password?</a>
                        </label> --}}
                        </div>
                        <div class="form-control mt-6">
                            <button class="btn btn-primary">Login</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
            document.getElementById("logo-aqila").setAttribute("src",
                "{{ asset('img/icon/aqila-kost-logo-white.png') }}"
            )
        }

        if (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) {
            document.getElementById("logo-aqila").setAttribute("src",
                "{{ asset('img/icon/aqila-kost-logo.png') }}")
        }

        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {
            const newColorScheme = event.matches ? "dark" : "light";

            if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                document.getElementById("logo-aqila").setAttribute("src",
                    "{{ asset('img/icon/aqila-kost-logo-white.png') }}"
                )
            }

            if (window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches) {
                document.getElementById("logo-aqila").setAttribute("src",
                    "{{ asset('img/icon/aqila-kost-logo.png') }}")
            }
        });
    </script>
@endsection
