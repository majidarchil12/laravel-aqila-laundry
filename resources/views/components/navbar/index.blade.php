<div class="navbar bg-base-100">
    <div class="navbar-start">
        <div class="dropdown">
            <label tabindex="0" class="btn btn-ghost lg:hidden">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" />
                </svg>
            </label>
            <ul tabindex="0" class="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52">
                <li><a>Dashboard</a></li>
                <li>
                    <a href="#">Manajemen</a>
                    <ul class="p-2">
                        <li><a>Pengguna</a></li>
                        <li><a>Item</a></li>
                    </ul>
                </li>
                <li><a>Transaksi</a></li>
            </ul>
        </div>
        <a class="btn btn-ghost normal-case text-xl">Aqila Laundry</a>
    </div>
    <div class="navbar-center hidden lg:flex">
        <ul class="menu menu-horizontal px-1">
            <li><a>Dashboard</a></li>
            <li tabindex="0">
                <details>
                    <summary>Manajemen</summary>
                    <ul class="p-2">
                        <li><a>Pengguna</a></li>
                        <li><a>Item</a></li>
                    </ul>
                </details>
            </li>
            <li><a>Transaksi</a></li>
        </ul>
    </div>
    <div class="navbar-end">
        <a class="btn" onclick="location.href(`http://127.0.0.1:8000`)">Logout</a>
    </div>
</div>
